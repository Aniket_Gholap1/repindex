

'use strict';

angular.module('app.profile').factory('ProfileResources', function ($http, $q, APP_CONFIG, httpApiConfig) {
    var dfd = $q.defer();

    return {
        // get_Admins: $resource(APP_CONFIG.apiRootUrl + "/Admin/adminApi", {}, {
        //     'query': { method: 'get', isArray: true },
        //     'add': { method: 'POST', url: apiUrl.url + '/Admin/adminApi/create', isArray: true },
        //     'delete': { method: 'POST', url: apiUrl.url + '/Admin/adminApi/delete' },
        //     'update': { method: 'POST', url: apiUrl.url + '/Admin/adminApi/update' }
        // }),

        editVendor: function (VendorInfo) {
            var UPDATE_URL = APP_CONFIG.apiRootUrl + "/Consumer/updateVendorApi";
            return $http.post(UPDATE_URL, VendorInfo, httpApiConfig);
        },
    }
});
