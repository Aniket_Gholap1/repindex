

'use strict';

angular.module('app.roles').factory('RoleResources', function ($http, $q, APP_CONFIG, httpApiConfig) {
    var dfd = $q.defer();

    return {
        roles: function () {
            var ROLE_URL = APP_CONFIG.apiRootUrl + "/roles";
            return $http.get(ROLE_URL);
        },
        addRole: function (query) {
            var ADDROLE_URL = APP_CONFIG.apiRootUrl + "/roles";
            return $http.post(ADDROLE_URL, query, httpApiConfig);
        },
        editRole: function (query) {
            var EDITROLE_URL = APP_CONFIG.apiRootUrl + "/roles/" + query.id;
            return $http.put(EDITROLE_URL, query, httpApiConfig);
        },
        deleteRole: function (query) {
            var DELETEROLE_URL = APP_CONFIG.apiRootUrl + "/roles/" + query.id;
            return $http.delete(DELETEROLE_URL, query, httpApiConfig);
        },
        permission: function (query) {
            var ACCESS_URL = APP_CONFIG.apiRootUrl + "/permission";
            return $http.post(ACCESS_URL, query, httpApiConfig);
        },
        savePermission: function (query) {
            var SAVEROLE_URL = APP_CONFIG.apiRootUrl + "/permission/"+ query.id;
            return $http.put(SAVEROLE_URL, query, httpApiConfig);
        }

    }
});
