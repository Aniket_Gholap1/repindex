'use strict';

angular.module('app.layout').controller('LayoutCtrl', function ($scope, $rootScope, $state) {


    $scope.currentUser = JSON.parse(localStorage.getItem('userObject'));
    $scope.permission = JSON.parse(localStorage.getItem('permission'));
    if ($scope.currentUser != null)
        $scope.authenticatedUser = true;
    $scope.home = function () {
        $state.go('app.home');
    }
    $scope.signOut = function () {
        localStorage.clear();
        $scope.authenticatedUser = false;
       window.location.href = "../user/#/home"
        
    }
});