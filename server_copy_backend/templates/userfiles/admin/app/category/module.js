"use strict";


angular.module('app.category', ['ui.router'])
.config(function ($stateProvider) {

    $stateProvider
        .state('app.category', {
            url: '/category',
            data: {
                title: 'Blank'
            },
            views: {
                "content@app": {
                    templateUrl: 'app/category/views/category.html',
                    controller: 'CategoryCtrl'
                }
            }
        })
});
