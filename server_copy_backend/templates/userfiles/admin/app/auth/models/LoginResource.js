

'use strict';

angular.module('app.auth').factory('LoginResource', function ($http, $q, APP_CONFIG, httpApiConfig) {
    var dfd = $q.defer();

    return {

        login: function (query) {
            var loginURL = APP_CONFIG.apiRootUrl + "/Register/loginApi";
            return $http.post(loginURL, query, httpApiConfig);
        },
        forgotpwd: function (forgotpasswordInfo) {
            var forgotpasswordURL = APP_CONFIG.apiRootUrl + "/Register/passwordApi";
            return $http.post(forgotpasswordURL, forgotpasswordInfo, httpApiConfig);
        },

    }
});
