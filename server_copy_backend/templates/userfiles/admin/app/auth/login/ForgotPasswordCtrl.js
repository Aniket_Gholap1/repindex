angular.module('app.auth').controller('ForgotPasswordCtrl', function ($scope, $state, LoginResource) {


    $scope.invalid = false;
    $scope.valid = false;
    $scope.forgot = function (forgotPassword) {
        console.log('forgotPassword');
        var forgotReq = LoginResource.forgotpwd(forgotPassword);
        forgotReq.success(function (forgotResponse) {
            if (forgotResponse.success) {
                $scope.valid = true;
                $state.go('login');
            }
            else {
                $scope.invalid = true;
                //$state.go('app.login');
            }

        })
    };

    /* $scope.submitForm = function (loginData) {
 
         console.log("I am entering the inside");
         var loginReq = LoginResource.login(loginData);
         loginReq.success(function (loginResponse) {
             if (loginResponse.success) {
                 $state.go('app.home');
             }
             else {
                 $state.go('app.login');
             }
 
         })
     };*/

})