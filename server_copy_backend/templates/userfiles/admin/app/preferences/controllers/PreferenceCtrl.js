'use strict';

angular.module('app.preferences').controller('PreferenceCtrl', function ($scope, $state, $parse, DTOptionsBuilder, PreferenceResources) {

    // $scope.currentUser = JSON.parse(localStorage.getItem('userObject'));

        var loadmodules = function () {
        console.log("API to bring roles")
        var modulesRequest = PreferenceResources.modules();
        modulesRequest.success(function (modulesResponse) {
        $scope.modules = modulesResponse.modules;
        console.log($scope.modules)
        })
    }
     loadmodules();
     
    // $scope.modules = [
    //     { id: "1", name: "Hotel", isSelected: true },
    //     {
    //         id: "2", name: "Wellness & Spa", isSelected: true,
    //         // "subModules": [
    //         //     { "id": "1", "name": "Spa1", "isSelected": true },
    //         //     { "id": "2", "name": "Spa2", "isSelected": true },
    //         //     { "id": "3", "name": "Wellness", "isSelected": true }
    //         // ]
    //     },
    //     {
    //         id: "3", name: "Advertisement", isSelected: true,
    //         "subModules": [
    //             {  "name": "Entertainment", "isSelected": true },
    //             { "name": "Education", "isSelected": true },
    //             {  "name": "Aquisition", "isSelected": true }
    //         ]
    //     }]

    $scope.moduleClikced = function (module, index) {
        var showSubModuleIndex = 'showSubModule' + index;
        var model = $parse(showSubModuleIndex);

        if (module.isSelected) {
            document.getElementById('subModule' + index).innerHTML = document.getElementById('subMenuItems' + index).innerHTML;
            //$("#subModule"+index).innerHTML = $("#subMenuItems"+index).innerHTML;
            model.assign($scope, true);
        } else {
            model.assign($scope, false);
        }
    },

        $scope.standardOptions = DTOptionsBuilder.newOptions()
            .withOption('bFilter', true)
            .withOption('bLengthChange', true)
            .withOption('bAutoWidth', false)
            .withOption('bPaginate', true)
            .withOption('bInfo', false)
            .withOption('bInfo', false)
            .withDOM("<'dt-toolbar'<'col-xs-12 col-sm-6'l><'col-sm-6 col-xs-12 hidden-xs'f>r>" +
            "t" +
            "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>")
            // .withDOM('frtrip')
            .withLanguage({
                "sSearch": "<span class='input-group-addon input-sm'><i class='glyphicon glyphicon-search'></i></span> "
            })
            .withBootstrap();
});