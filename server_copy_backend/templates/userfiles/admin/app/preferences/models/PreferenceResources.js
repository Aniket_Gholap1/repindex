

'use strict';

angular.module('app.preferences').factory('PreferenceResources', function ($http, $q, APP_CONFIG, httpApiConfig) {
    var dfd = $q.defer();

    return {
        // get_Admins: $resource(APP_CONFIG.apiRootUrl + "/Admin/adminApi", {}, {
        //     'query': { method: 'get', isArray: true },
        //     'add': { method: 'POST', url: apiUrl.url + '/Admin/adminApi/create', isArray: true },
        //     'delete': { method: 'POST', url: apiUrl.url + '/Admin/adminApi/delete' },
        //     'update': { method: 'POST', url: apiUrl.url + '/Admin/adminApi/update' }
        // }),

        modules: function () {
            var MODULE_URL = APP_CONFIG.apiRootUrl + "/modules";
            return $http.get(MODULE_URL);
        },
    }
});
