

'use strict';

angular.module('app.consumers').factory('ConsumerResources', function ($http, $q, APP_CONFIG, httpApiConfig) {
    var dfd = $q.defer();

    return {
        // get_Admins: $resource(APP_CONFIG.apiRootUrl + "/Admin/adminApi", {}, {
        //     'query': { method: 'get', isArray: true },
        //     'add': { method: 'POST', url: apiUrl.url + '/Admin/adminApi/create', isArray: true },
        //     'delete': { method: 'POST', url: apiUrl.url + '/Admin/adminApi/delete' },
        //     'update': { method: 'POST', url: apiUrl.url + '/Admin/adminApi/update' }
        // }),
        roles: function () {
            var ROLE_URL = APP_CONFIG.apiRootUrl + "/Roles/roleApi";
            return $http.get(ROLE_URL);
        },
        consumers: function () {
            var CONSUMER_URL = APP_CONFIG.apiRootUrl + "/consumer";
            return $http.get(CONSUMER_URL);
        },
        editConsumer: function (query) {
            var UPDATE_URL = APP_CONFIG.apiRootUrl + "/consumer/" + query.id;
            return $http.put(UPDATE_URL, query, httpApiConfig);
        },
        deleteConsumer: function (query) {
            var DELETE_URL = APP_CONFIG.apiRootUrl + "/consumer/" + query.id;
            return $http.delete(DELETE_URL, query, httpApiConfig);

        },
        changeStatus: function (query) {
            var STATUS_URL = APP_CONFIG.apiRootUrl + "/Register/changeStatus";
            return $http.post(STATUS_URL, query, httpApiConfig);
        },
        emailVerify: function (query) {
            var STATUS_URL = APP_CONFIG.apiRootUrl + "/Register/emailVerify";
            return $http.post(STATUS_URL, query, httpApiConfig);
        },
        sendEmail: function (query) {
            var EMAIL_URL = APP_CONFIG.apiRootUrl + "/Register/sendEmail";
            return $http.post(EMAIL_URL, query, httpApiConfig);
        }

    }
});
