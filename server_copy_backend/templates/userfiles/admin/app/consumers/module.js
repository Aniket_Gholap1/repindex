"use strict";

angular.module('app.consumers', [ 'ui.router', 'datatables', 'datatables.bootstrap']);

angular.module('app.consumers', ['ui.router'])
.config(function ($stateProvider) {

    $stateProvider
        .state('app.consumers', {
            url: '/consumers',
            data: {
                title: 'Consumer'
            },
            views: {
                "content@app": {
                    templateUrl: 'app/consumers/views/consumers.html',
                    controller: 'ConsumerCtrl'
                }
            }
        })
});
