

'use strict';

angular.module('app.admin').factory('AdminResource', function ($http, $q, APP_CONFIG, httpApiConfig) {
    var dfd = $q.defer();

    return {
        // get_Admins: $resource(APP_CONFIG.apiRootUrl + "/Admin/adminApi", {}, {
        //     'query': { method: 'get', isArray: true },
        //     'add': { method: 'POST', url: apiUrl.url + '/Admin/adminApi/create', isArray: true },
        //     'delete': { method: 'POST', url: apiUrl.url + '/Admin/adminApi/delete' },
        //     'update': { method: 'POST', url: apiUrl.url + '/Admin/adminApi/update' }
        // }),

        admins: function () {
            var ADMIN_URL = APP_CONFIG.apiRootUrl + "/admin";
            return $http.get(ADMIN_URL);
        },
        addAdmin: function (query) {
            var ADD_URL = APP_CONFIG.apiRootUrl + "/admin";
            return $http.post(ADD_URL, query, httpApiConfig);
        },
        editAdmin: function (query) {
            var EDIT_URL = APP_CONFIG.apiRootUrl + "/admin/" + query.id;
            return $http.put(EDIT_URL, query, httpApiConfig);
        },
        deleteAdmin: function (query) {
            var DELETE_URL = APP_CONFIG.apiRootUrl +"/admin/" + query.id;
            return $http.delete(DELETE_URL, query, httpApiConfig);
        
        },
        roles: function () {
            var ROLE_URL = APP_CONFIG.apiRootUrl + "/roles";
            return $http.get(ROLE_URL);
        },
        changeStatus: function (query) {
            var STATUS_URL = APP_CONFIG.apiRootUrl + "/Register/changeStatus";
            return $http.post(STATUS_URL, query, httpApiConfig);
        },
        statusInactive: function (query) {
            var STATUS_URL = APP_CONFIG.apiRootUrl + "/Register/statusInActive";
            return $http.post(STATUS_URL, query, httpApiConfig);
        },
        sendEmail: function (query) {
            var EMAIL_URL = APP_CONFIG.apiRootUrl + "/Register/sendEmail";
            return $http.post(EMAIL_URL, query, httpApiConfig);
        }
    }
});
