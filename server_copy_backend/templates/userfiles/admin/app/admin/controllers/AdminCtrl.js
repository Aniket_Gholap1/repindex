'use strict';

angular.module('app.admin').controller('AdminCtrl', function ($scope, $state, AdminResource, DTOptionsBuilder) {


    var roleRequest = AdminResource.roles();
    roleRequest.success(function (roleResponse) {
        if (roleResponse.success) {
            $scope.roles = roleResponse.result;
        }
        else {
            $state.go('app.admin');
        }
    })

    var loadAdmins = function () {
        console.log("API to bring admins")
        var adminRequest = AdminResource.admins();
        adminRequest.success(function (adminResponse) {
            if (adminResponse.success) {
                $scope.admins = adminResponse.result;
            }
            // else if ($scope.admins.status == 'inactive') {
            //     $scope.inActiveUser = true;
            // }
        })
    }

    loadAdmins();



    $scope.setAdmin = function (admin, index) {
        $scope.currentAdminIndex = index;
        $scope.currentAdmin = admin;
    }

    $scope.changeStatus = function (admin) {
        var query = {};
        query.id = admin.id;
        var changeStatusRequest = AdminResource.changeStatus(query);
        changeStatusRequest.success(function (changeStatusResponse) {
            loadAdmins();

        })
    }

    $scope.sendEmail = function (admin) {
        var query = {};
        query.id = admin.id;
        var sendEmailRequest = AdminResource.sendEmail(query);
        sendEmailRequest.success(function (sendEmailResponse) {
            if (sendEmailResponse.success) {
                $scope.result = sendEmailResponse.result;

            }
            else {
                $scope.result = sendEmailResponse.result;

            }
        })
    }


    $scope.addAdmin = function () {
        $scope.newAdmin = {};
        $('#addAdminPopup').modal('show');
    }

    $scope.editAdmin = function () {
        if ($scope.currentAdmin != null) {
            $scope.newAdmin = $scope.currentAdmin;
            $('#addAdminPopup').modal('show');
        }
        // else {
        //     $scope.modalInstance = $modal.open({
        //         templateUrl: 'app/modules/data-gov/views/noSelection.modal.view.html',
        //         controller: 'removeModalCtrl',
        //         controllerAs: 'vm',
        //         resolve: {
        //             model: function () { return 'Admin' },
        //             action: function () { return 'Edit'; }
        //         }
        //     });
        // }
    }


    $scope.deleteAdmin = function () {
        if ($scope.currentAdmin != null) {
            $('#deleteAdminPopup').modal('show');
        }
    }

    $scope.adminDelete = function () {
        console.log("API to bring admins")
        var query = {};
        query.id = $scope.currentAdmin.id;
        var adminRequest = AdminResource.deleteAdmin(query);
        adminRequest.success(function (adminResponse) {
            if (adminResponse.success) {
                loadAdmins();
                $('#deleteAdminPopup').modal('hide');
                $scope.result = adminResponse.result;

            }
        })
    }

    $scope.submit = function (newAdmin) {
        if ($scope.adminAction == 'Add') {
            var addAdminReq = AdminResource.addAdmin(newAdmin);
            addAdminReq.success(function (addAdminResponse) {
                if (addAdminResponse.success) {
                    loadAdmins();
                    //   $('#successpopup').modal('show');
                    $scope.success = true;
                    $scope.result = addAdminResponse.result;
                }
                else {
                    $scope.failed = true;
                    $scope.result = addAdminResponse.result;
                }
            })
        }
        if ($scope.adminAction == 'Edit') {
            var editAdminReq = AdminResource.editAdmin(newAdmin);
            editAdminReq.success(function (editAdminResponse) {
                if (editAdminResponse.success) {
                    loadAdmins();
                    $scope.success = true;
                    $scope.result = editAdminResponse.result;
                }
                else {
                    $scope.failed = true;
                    $scope.result = editAdminResponse.result;
                }
            })
        }
        $scope.newAdmin = {};
        $scope.currentAdminIndex = null;
        $('#addAdminPopup').modal('hide');
    }

    $scope.cancel = function () {
        $scope.currentAdmin = null;
        $scope.newAdmin = {};
        $scope.currentAdmin = null;
        $('#addAdminPopup').modal('hide');
        $('#deleteAdminPopup').modal('hide');
    }


    $scope.standardOptions = DTOptionsBuilder.newOptions()
        .withOption('bFilter', true)
        .withOption('bLengthChange', true)
        .withOption('bAutoWidth', false)
        .withOption('bPaginate', true)
        .withOption('bInfo', false)
        .withOption('bInfo', false)
        .withDOM("<'dt-toolbar'<'col-xs-12 col-sm-6'l><'col-sm-6 col-xs-12 hidden-xs'f>r>" +
        "t" +
        "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>")
        // .withDOM('frtrip')
        .withLanguage({
            "sSearch": "<span class='input-group-addon input-sm'><i class='glyphicon glyphicon-search'></i></span> "
        })
        .withBootstrap();

});
