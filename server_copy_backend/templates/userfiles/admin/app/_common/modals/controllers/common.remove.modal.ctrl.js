'use strict';

angular.module('SmartAdmin.Forms').controller('CommonSmartFormRemoveModalCtrl', function ($scope, $http, $modalInstance, title, description) {

	$scope.title = title;
	$scope.description = description;
	$scope.closeModal = function () {
		$modalInstance.dismiss('cancel');
	};
	$scope.submit = function () {
		$modalInstance.close();
	}
});




