from django.conf.urls import url
from django.contrib import admin
from repindexapp import views
from django.conf.urls import include, url
urlpatterns = [
    url(r'^Register/(?P<userid>\d+)/$', views.clientregsiter),
    url(r'^login/$', views.Login),
    url(r'^getallclient/(?P<userid>\d+)/$', views.getAllClient),
    url(r'^deleteUser/$', views.deleteUser),
    url(r'^inactiveUser/(?P<user_id>\d+)/$', views.inactiveUser),
    url(r'^getuserbyid/(?P<user_id>\d+)/$', views.getuserbyid),
    url(r'^activateUser/(?P<user_id>\d+)/$', views.activateUser),
    url(r'^updateClientInfo/(?P<id>\d+)/$', views.updateClientInfo),
    url(r'^addproject/(?P<user_id>\d+)/$', views.addProject),
    url(r'^getproject/(?P<user_id>\d+)/$', views.getProject),
    url(r'^renameproject/$', views.renameproject),
    url(r'^deleteproject/$', views.deleteproject),
    url(r'^getprojectDetails/(?P<project_id>\d+)/$', views.getprojectDetails),
    url(r'^addFolder/(?P<project_id>\d+)/(?P<user_id>\d+)/$', views.addFolder),
    url(r'^renameFolder/(?P<folder_id>\d+)/$', views.renamefolder),
    url(r'^deleteFolder/(?P<folder_id>\d+)/$', views.deletefolder),
    url(r'^Savetextlist/(?P<project_id>\d+)/(?P<user_id>\d+)/(?P<folder_id>\d+)$', views.Savetextlist),
    url(r'^Savetextlist/(?P<project_id>\d+)/(?P<user_id>\d+)/(?P<folder_id>\d+)/(?P<access_token>[\w\.-]+)/$', views.Savetextlist),
    url(r'^saveConceptlist/(?P<project_id>\d+)/(?P<user_id>\d+)/(?P<folder_id>\d+)/$', views.saveConceptlist),
    url(r'^concordanceline/(?P<project_id>\d+)$', views.concordanceline),
    url(r'^import-stop-list/$', views.importstoplist),
    url(r'^selfcordoncordance/(?P<project_id>\d+)/(?P<user_id>\d+)/$', views.selfcordoncordance),
    url(r'^clusterquery/(?P<project_id>\d+)/(?P<user_id>\d+)/$', views.clusterquery),
    url(r'^getalltextlist/(?P<project_id>\d+)/$', views.getalltextlist),
    url(r'^getconceptlist/(?P<project_id>\d+)/$', views.getconceptlist),
    url(r'^getfilecontes/(?P<file_id>\d+)/$', views.getfilecontes),
    url(r'^renameconcept/$', views.renameconcept),
    url(r'^deleteconcept/$', views.deleteconcept),
    url(r'^frequencycount/$', views.frequencycount),
    url(r'^stoplistdata/$', views.getstoplistdata),
    url(r'^savedocumentdata/$', views.savedocumentdata),
    url(r'^getAlladmin/$', views.getAlladmin),
    url(r'^adminregsiter/$', views.adminregsiter),
    url(r'^rename-stoplist/$', views.renameStopList),
    url(r'^delete-stoplist/(?P<stoplist_id>\d+)/$', views.deleteStopList),
    url(r'^updatesuperuser/(?P<id>\d+)/$', views.updatesuperuser),
    url(r'^dropboxapi/$', views.dropboxapi),
    url(r'^wordexport/$', views.wordexport),
    url(r'^addDefination/$', views.addDefination),
    url(r'^getDefination/(?P<id>\d+)/$',views.getDefination),
    url(r'^exportdoc/$',views.exportdoc),
    url(r'^download-document/(?P<file_id>\d+)/$',views.downloadDocument),
    url(r'^drop/$',views.drop),
    url(r'^forgotpassword/$',views.forgotpassword),
    url(r'^changepassword/(?P<token>[\w\.-]+)/$',views.changepassword),
    url(r'^delete-text-list/(?P<textlist_id>\d+)/$', views.deleteTextList),
    url(r'^import-master-list/(?P<user_id>\d+)/$', views.importmasterlist),
    url(r'^delete-master-list/(?P<id>\d+)/$', views.deleteMasterList),
    url(r'^rename-master-list/$', views.updateMasterList),
    url(r'^get-master-list/$', views.getmasterlist),
    url(r'^delete-multiple/$', views.deleteMultipleTextlist),
    url(r'^delete-multiple-concept/$', views.deleteMultipleConceptlist),
    url(r'^get-value/$', views.getValue),
    url(r'^create-folder/(?P<project_id>\d+)/(?P<user_id>\d+)/$', views.createFolder),
    url(r'^get-concept-folder/(?P<project_id>\d+)/$', views.getConceptFolder),
    url(r'^rename-concept-folder/(?P<folder_id>\d+)/$', views.renameConceptFolder),
    url(r'^delete-concept-folder/(?P<folder_id>\d+)/$', views.deleteConceptFolder),
    url(r'^addsubFolder/(?P<user_id>\d+)/(?P<project_id>\d+)/$', views.addsubFolder),
    url(r'^add-url/(?P<project_id>\d+)/$', views.addUrl),
    url(r'^get-project-data/(?P<project_id>\d+)/$', views.getprojectData),
    url(r'^concept-status/(?P<user_id>\d+)/$', views.conceptStatus),
    url(r'^stoplist-status/(?P<user_id>\d+)/$', views.stopListStatus),
    url(r'^get-urls/(?P<project_id>\d+)/$', views.getUrls),
    url(r'^delete-url/(?P<url_id>\d+)/$',views.deleteUrl),
    url(r'^document-comparison/$', views.dataComparision),
    url(r'^get-url-data/$', views.getSelectedUrlData), 
    url(r'^get-document-data/$', views.getDocumentData),
    
    
]


